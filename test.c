#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include<ctype.h>
#include<math.h> 
#include"tp1.c"


int test_getNombreLi();
int do_test();

int do_test()
{
   test_getNombreLi(); 
}



int test_getNombreLi()
{
    char chemin[] = {'N','N'};
    if (getNombreLi(chemin) != 2) 
        printf("Le nombre de li attendu est 2, le nombre trouve est : %i \n", getNombreLi(chemin));
    char chemin2 = {'N','N','N','S','N','S','N','S','N'};

    if (getNombreLi(chemin2) != 3) 
        printf("Le nombre de li attendu est 3, le nombre trouve est : %i \n", getNombreLi(chemin2));
    
    return 0;
}
