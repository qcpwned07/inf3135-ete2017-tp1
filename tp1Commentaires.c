#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include<ctype.h>
#include<math.h> 
// ---------------- //
//    Constantes    //
// ---------------- //

#define NB_ARG      5
#define MAX_LI      10
#define MAX_COL     15
#define MAX_CHEMIN  41                  // Chemin de longueur max de 50,+le caractere null
#define NB_CASES    150

// ---------------- //
//    STRUCTURES    //
// ---------------- //

struct Rectangle {                      // Un rectangle à coordonnées entières
    int xmin;                           // L'abscisse minimale
    int xmax;                           // L'abscisse maximale
    int ymin;                           // L'ordonnée minimale
    int ymax;                           // L'ordonnée maximale
};
struct Dessin {
    char cases[MAX_LI][MAX_COL];
};
/*
struct Dessin {                         // Un dessin
    unsigned int nbLignes;              // Le nombre de lignes
    unsigned int nbColonnes;            // Le nombre de colonnes
    char cases[MAX_LI][MAX_COL];        // Les cases du dessin
    char occupe;                        // Le caractère pour une case occupée
    char vide;                          // Le caractère pour une case vide
    char intersection;                  // Le caractère pour une intersection
};
*/
struct Chemin {                         // Un chemin
    struct Rectangle rectangle;         // Le rectangle dans lequel s'inscrit le chemin
    char deplacements[MAX_CHEMIN + 1];  // Les déplacements
};

// ------------ //
// DECLARATIONS //
// ------------ //

// Methode qui affiche err_mssg[] et arrete le programme avec le codei d'erreur : code
void afficherErreur(char err_mssg [], int code);

//Fonction qui deduit le nombre de colonne que remplira le chemin et le retourne
//@param chemin : chemin a dessiner passe au programme par l'utilisateur
int getNombreCol(char* chemin);

//Fonction qui deduit la colonne de la premiere case occupe, en fonction du chemin
//@param chemin : chemin a dessiner passe au programme par l'utilisateur
int getColDepart(char* chemin);

//Fonction qui deduit la ligne de la premiere case occupe, en fonction du chemin
//@param chemin : chemin a dessiner passe au programme par l'utilisateur
int getLiDepart(char* chemin);

//Fonction qui deduit le nombre de ligne que remplira le chemin et le retourne
//@param chemin : chemin a dessiner passe au programme par l'utilisateur
int getNombreLi(char* chemin);

//Methode qui dessine un chemin dans le tableau passe en parametre.
//@param chemin : chemin a dessiner passe au programme par l'utilisateur
void creerChemin(char chemin[MAX_CHEMIN], char tableau[MAX_LI][MAX_COL]);

//Methode qui affiche le tableau passe en parametre.
//Cette methode affiche toute les cases du tableau, sauf celles qui sont = \0
void afficherTab(char tableau[MAX_LI][MAX_COL] );

//Methode qui rempli un tableau 2d de taille nbCol x nbLi avec le char parametre vide,
//et qui met des caracteres vides ( \0 ) dans les  autres cases du tableau.
void  initTableau(int nbLi, int nbCol, char vide, char tableau[MAX_LI][MAX_COL]);

//Methode qui parcourt le tableau passe en parametre et qui change les cases occupees
//qui remplisse les conditions pour etres des intersections (au moins 3 voisines sont
//occupees)
void placerIntersections (char inter, char occupe,  char tableau[MAX_LI][MAX_COL]);

//Methode qui trace le chemin passe en parametre dans le tableau egalement passe en param
//avec le char occupe, et qui prend ses origines a xDepart et yDepart
void tracer(int yDepart, int xDepart, char occupe,  
            char chemin[], char tableau[MAX_LI][MAX_COL]);
// ------------ //
//  PROGRAMME   //
// ------------ //
int main (int argc, char * argv[]) 
{
    int i = 0;                  // Iterateur pour boucles for
    int nbCol, nbLi;            // Nombre de lignes et colonnes dans le tableau final
    int liDepart, colDepart;    // Origine
    char vide, occupe, inter;   // Caracteres a utiliser
    char chemin[MAX_CHEMIN];    // Chemin a tracer
    char tableau[MAX_LI][MAX_COL]; 

    if (argc == NB_ARG) {
        vide   = argv[2][0];
        occupe = argv[1][0];
        inter   = argv[3][0];
    
        if (strlen(argv[4]) >= MAX_CHEMIN)
            afficherErreur("la longueur ne doit pas depasser 40",5);
        for(i=0;i< (int)strlen(argv[4]); i++)
            chemin[i] = argv[4][i];
        while(i < MAX_CHEMIN)
            chemin[i++] = '\0';
    }
    if (argc != NB_ARG)         // Valider les argument (validation de base)
        afficherErreur("le nombre d'arguments est invalide", 1);
    else if (vide==occupe || vide == inter || occupe == inter)
        afficherErreur("le caractere de case vide doit etre distinct",3);

    for(i=1;i<NB_ARG-1;i++)     // Valider la longueur des 3 premiers arg
        if(strlen(argv[i]) != 1)
            afficherErreur("les cases doivent etre identifiees par des caracteres",2);

    //printf("%d\n",(int)strlen(chemin));
    //printf("%d\n",(int)strlen(argv[4]));
    //
    //for (i=0; i<strlen(chemin)-1;i++) // Verifier que le chemin est compose de NSOE
    //    printf("%c\n",chemin[i]);
    //printf("\n");
    for (i=0; i<(int)strlen(chemin);i++) // Verifier que le chemin est compose de NSOE
    {
        if( chemin[i]!='N' && chemin[i]!='S' && chemin[i]!='W' && chemin[i]!='E' && chemin[i]!='\0')
            afficherErreur("les deplacements doivent etre E, N, S ou W ", 4);
        
    }

    //Appel des fonctions afin d'executer le programme
    
    nbCol = getNombreCol(chemin);
    nbLi = getNombreLi(chemin);
    colDepart = getColDepart(chemin);
    liDepart = getLiDepart(chemin);

    initTableau(nbLi, nbCol, vide, tableau);
    
    //printf("Main -- nCol:%i, nLi %i\n", nbCol,nbLi); 
    //rintf("Main -- DCol:%i, DLi %i\n", colDepart,liDepart); 

    tracer(liDepart,colDepart,occupe, chemin, tableau);

    placerIntersections(inter,occupe,tableau);
    afficherTab(tableau);

    return 0;
}

void tracer(int yDepart, int xDepart, char occupe,
            char chemin[], char tableau[MAX_LI][MAX_COL])
{
    int i,x,y;

    x = xDepart;
    y = yDepart;
    
    //printf("Tracer : Depart li %i, Depart col %i\n", y,x);
    //Ajustement pour la premiere case
    //x += chemin[0] == 'E' ? -1 : chemin[0]=='O' ? 1 : 0;
    //y += chemin[0] == 'N' ? -1 : chemin[0]=='S' ? 1 : 0;

    tableau[y][x] = occupe;

    // ------------------------------ DEBUG -------------------------??
    //Attention au bug si chemin = 40
    for (i=0;i<(int)strlen(chemin);i++){
        if(chemin[i]=='N')
            y++;
        else if(chemin[i] =='S')
            y--;
        else if(chemin[i] =='E')
            x++;
        else if(chemin[i] =='W'){
            x--;
        }
        tableau[y][x] = occupe;
    }
}

void placerIntersections (char inter, char occupe, char tableau[MAX_LI][MAX_COL])
{
    int i,j;
    int nOcc;
    for (i=0; i<MAX_LI; i++) {
        for (j=0;j<MAX_COL; j++){
            nOcc=0;
            if(j+1 < MAX_COL)
                if(tableau[i][j+1] == occupe || tableau[i][j+1] == inter)
                    if (tableau[i][j] == occupe )
                        nOcc++;
            if(j-1 >= 0)
                if(tableau[i][j-1] == occupe || tableau[i][j-1] == inter)
                    if (tableau[i][j] == occupe )
                        nOcc++;
            if(i+1 < MAX_LI)
                if(tableau[i+1][j] == occupe || tableau[i+1][j] == inter)
                    if (tableau[i][j] == occupe )
                        nOcc++;
            if(i-1 >= 0)
                if(tableau[i-1][j] == occupe || tableau[i-1][j] == inter)
                    if (tableau[i][j] == occupe )
                        nOcc++;
            if (nOcc > 2)
                tableau[i][j] = inter;
        }
    }
}

int getNombreCol(char* chemin)
{
    int nombreCol = 1, i, max=1, min=1;

    for(i=0;i<(int)strlen(chemin); i++){
        if(chemin[i] == 'W'){
            nombreCol--;
            if (min>nombreCol)
                min=nombreCol;
        }
        else if(chemin[i] == 'E'){
            nombreCol++;
            if (max<nombreCol)
                max = nombreCol;
        }
    }

    if (min<1) 
        --min;
    else 
        min=0;
    nombreCol =  max - min;
    if(nombreCol > MAX_COL)
        afficherErreur("la largeur ne doit pas depasser 15", 7);
    // ------------------------ DEBUG --------------------- // 
    //printf("getNombreCol: max %i, min %i\n", max, min);
    return nombreCol;
}

int getNombreLi(char* chemin)
{
    int nombreLi = 1, i, max=1,min=1;

    for(i=0;i<(int)strlen(chemin); i++){
        if(chemin[i] == 'N'){
            nombreLi++;
            if (max<nombreLi)
                max=nombreLi;
        }
        else if(chemin[i] == 'S'){
            nombreLi--;
            if (min>nombreLi)
                min=nombreLi;
            }
    }

    //printf("getNombreLi : max : %d  min : %d\n",max, min);
    //min = (min < 1) ? --min : 0;
    if (min<1) 
        --min;
    else 
        min=0;
    nombreLi =  max - min;
    //printf("getNombreLi : max : %d  min : %d\n",max, min);
    if(nombreLi > MAX_LI)
        afficherErreur("la hauteur ne doit pas depasser 10", 6);
   
    return nombreLi;
}

int getLiDepart(char * chemin)
{
    int i;
    int min=0, actuel=0;

    for(i=0;i<(int)strlen(chemin); i++){
        if(chemin[i] == 'N')
            actuel++;
        else if(chemin[i] == 'S'){
            actuel--;
            if (min>actuel)
                min=actuel;
            }
    }
    //printf("getLiDepart : min %i, actuel %i\n", min,actuel);
    return -1*min ;
}


int getColDepart(char * chemin)
{
    int i;
    int min=0, actuel=0;
    
    for(i=0;i<(int)strlen(chemin); i++){
        if(chemin[i] == 'E')
            actuel++;
        else if(chemin[i] == 'W'){
            actuel--;
            if (min>actuel)
                min=actuel;
            }
    }
    return -1*min ;       
}



void initTableau(int nbLi, int nbCol, char vide, char tableau[MAX_LI][MAX_COL])
{
    int i,j;

    for (i=0;i<MAX_LI;i++)
       for(j=0;j<MAX_COL; j++)
           tableau[i][j] = '\0';

    for (i=0;i<nbLi;i++)
       for(j=0;j<nbCol; j++)
           tableau[i][j] = vide;
}

void afficherTab(char tableau[MAX_LI][MAX_COL])
{
    int i=0, j=0;

    for (i=MAX_LI-1;i>=0;i--) 
    {
        if (tableau[i][j]!='\0')
            printf("\n");
        for(j=0; j< MAX_COL; j++)
            if (tableau[i][j] != '\0' )
                printf("%c" , tableau[i][j]);
    }   
    printf("\n");
}


void afficherErreur (char err_mssg [], int code ) 
{
    fprintf(stdout, "Erreur: %s\n", err_mssg);
    exit(code);        
}


//*****************//
//  Question Prof  //
//*****************//
// 1.X  Messages d'erreur en enum?
// 2.X  Err_mssg -> Angliscisme?
// 3.X  Afficher les espaces?
// 4.X  Tableau 2 dimensions
// 5.x  Valider arguments dans la main?
// 6.X  Enlever toUpper dans la transcription de argv[4] dans chemin
// 7.
