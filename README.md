# Travail pratique 1
## Description
Ce travail vise a afficher un chemin dans un tableau (en 2 dimensions).
Il s'agit du premier travail pratique du cours sur le language C INF3135, donne a l'ete 2017 par M. Alexandre Blondin-Masse
## Auteur
Matthieu Picard 
PICM07129307

## Fonctionnement
Pour generer un executable :
    
    make

Pour lancer le programme :

    $ ./tp1 <o> <v> <i> <c> 

Ou :

\<o\> est le caractere qui represente les cases occupees

\<v\> est le caractere qui represente les cases vides

\<i\> est le caractere qui represente les cases d'intersection (lorsque le chemin se croise)

\<c\> est la chaine des directions fournie par l'utilisateur afin de tracer le chemin. Doit etre compose seulement des caractere suivant: 

*N
*S
*W
*E

### Exemples d'utilistation

#### Exemple #1
ENTREE :  

    $ ./tp1 + - o NNEESS

SORTIE :

    +++
    +-+
    +-+

#### Exemple #2

ENTREE :

    $ ./tp1 x . + NNEESSSSWWWWNNNEE

SORTIE :

    ..xxx
    xx+.x
    x.x.x
    x...x
    xxxxx



## Contenu du projet

* .gitignore : Fichier a ne pas etre telecharges sur le serveur
* Makefile : Makefile du projet
* tp1.c : code source 
* tp1Commentaires.c : code source, mais avec des commentaires plus detailles (retire puisque la majorite sont a des fins de developpement/superflus)
* test.py : fichier de tests
* test.c : petit fichier contenant les test de fonction (non complete puisque les test.py ont ete mis en ligne par la suite)

## References

Seule reference utilisee :

http://lacim.uqam.ca/~blondin/fr/inf3135-tp1

## Statut 

TP termine (dernier commit le 21/06 suite a ce que nous avions discute en classe concernant le README.md en raison du probleme de merge)

