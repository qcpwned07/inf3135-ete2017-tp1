
tp1: tp1.o
	gcc tp1.o -o tp1

tp1.o: tp1.c
	gcc -c tp1.c -o tp1.o

.PHONY: clean

clean:
	rm -f tp1.o tp1

test:
	python test.py
